import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import api from './services/api'

api.setDefaults();

ReactDOM.render(<App />, document.getElementById('root'));
