import React from 'react'

import './Pagination.scss'

const pageList = (pages) => {
  const elementList = []

  for (let index = 0; index < pages.total; index++) {
    let current = index + 1
    let isActive = (current === Number(pages.current))

    elementList.push(
      <button
        key={current}
        className={
          `Pagination__item
          ${isActive ? `Pagination__item--active`: ""}
          `
        }
        onClick={!isActive ? () => pages.callback(current) : null}>
        { current }
      </button>
    )
  }
  return elementList
}

function Pagination (props) {
  const {
    next,
    previous,
    pages
  } = props

  return (
    <div className="Pagination">
      { previous && <button onClick={previous} className="Pagination__item Pagination__item--jump">Previous</button> }
      { pageList(pages) }
      { next && <button onClick={next} className="Pagination__item Pagination__item--jump">Next</button> }
    </div>
  )
}

export default Pagination
