import React from 'react'

import Starships from '../../containers/Starships/Starships'
import List from '../List/List'

import './MainCharacter.scss'


function MainCharacter (props) {
  const {
    character,
    clearCharacter
  } = props

  const list = [
    { label: 'Name', value: character.name },
    { label: 'Birth year', value: character.birth_year },
    { label: 'Eye color', value: character.eye_color },
    { label: 'Gender', value: character.gender },
    { label: 'Hair color', value: character.hair_color },
    { label: 'Height', value: character.height },
    { label: 'Mass', value: ((character.mass !== 'unknown') ? `${character.mass}kg` : character.mass)  },
    { label: 'Skin color', value: character.skin_color }
  ]

  return (
    <div className="MainCharacter">
      <button className="MainCharacter__close" onClick={clearCharacter}>X</button>
      <div className="MainCharacter__pic">
        <div className="MainCharacter__avatar">
          <img
            src="https://starwarsblog.starwars.com/wp-content/uploads/2019/08/rise-of-skywalker-poster-tall.jpg"
            alt="Main character"/>
        </div>
      </div>
      { <List list={list}/>}
      { character.starships && <Starships starshipUrls={ character.starships } /> }
    </div>
  )
}

export default MainCharacter
