import React from 'react'

import './List.scss'

function List (props) {
  const {
    list
  } = props

  return (
    <div className="List__data">
      { list.map((item, index) => {
        return (
          <div key={index} className="List__item">
            <div className="List__label">{item.label}</div>
            <div className="List__value">
            { item.value }
            </div>
          </div>
        )
      }) }
    </div>
  )
}

export default List
