import axios from "axios";

class Api {
  static axios
  static next
  static previous
  static count = 0
  static perPage = 10

  static baseUrl = "https://swapi.co/api/"
  static resource = ""

  static setDefaults () {
    this.axios = axios.create({
      baseURL: this.baseUrl,
      timeout: 10000
    })
  }

  static parseEndpoint (endpoint) {
    if (!endpoint) return endpoint

    return endpoint.replace(this.baseUrl, "")
  }

  static getEndpoint (endpoint, page) {
    if (!page) {
      return endpoint
    }
    switch (page) {
      case "next":
        return (this.next && this.parseEndpoint(this.next)) || endpoint
      case "previous":
        return (this.previous && this.parseEndpoint(this.previous)) || endpoint
      default:
        return `${endpoint}/?page=${page}`
    }
  }

  static async getResource (endpoint, callback) {
    const { data } = await this.axios.get(this.parseEndpoint(endpoint))

    if (callback) callback(data)

    return data
  }

  static async getResourceList (callback, page = null) {
    const endpoint = this.getEndpoint(this.resource, page)

    const {
      data: {
        results: characters,
        next,
        previous,
        count
      }
    } = await this.axios.get(endpoint)

    this.next = next;
    this.previous = previous;
    this.count = count

    callback(characters)
  }

  static getNextResource(callback) {
    this.getResourceList(callback, "next")
  }

  static getPreviousResource(callback) {
    this.getResourceList(callback, "previous")
  }

  static getTotalPages() {
    return Math.ceil(this.count / this.perPage)
  }

  static getCurrentPage() {
    const previous = this.getEndpoint(this.previous)

    if (!previous) return 1

    const [, page] = previous.match(/page=(\d+)/)

    if (!page) return 1

    return Number(page) + 1
  }
}


export default Api
