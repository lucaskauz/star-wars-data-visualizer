import Api from "./api"

class StarshipApi extends Api {
  static resource = "/starships"
}

export default StarshipApi
