import Api from "./api"

class PeopleApi extends Api {
  static resource = "/people"
}

export default PeopleApi
