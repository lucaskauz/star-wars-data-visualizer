import React, { useEffect, useState } from 'react'

import List from '../../components/List/List'

import starshipsApi from '../../services/starships'

import './Starships.scss'

function Starships (props) {
  const [starships, setStarships] = useState([])

  const {
    starshipUrls
  } = props

  // Clear the starship list everytime the starshipUrls changes
  useEffect(() => {
    setStarships([])
  }, [ starshipUrls ])

  // Fetch and list starships
  useEffect(() => {
    if (!starshipUrls || starshipUrls.length < 1) return

    Promise.all(starshipUrls.map(async (starshipUrl) => {
      return await starshipsApi.getResource(starshipUrl)
    }))
    .then((data) => {
      console.log(data)
      setStarships(data)
    })

    return
  }, [ starshipUrls ])


  if (starships.length < 1) {
    console.log('entered', starships)
    return null
  } else {
    console.log('do not entered', starships)
  }

  return (
    <div className="Starships">
      <div className="Starships__title">Starships</div>
      { starships.map((starship, index) => {
        let list = [
          { label: 'Name', value: starship.name },
          { label: 'MGLT', value: starship.MGLT },
          { label: 'Cargo capacity', value: starship.cargo_capacity },
          { label: 'Consumables', value: starship.consumables },
          { label: 'Cost in credits', value: starship.cost_in_credits },
          { label: 'Crew', value: starship.crew },
          { label: 'Hyperdrive rating', value: starship.hyperdrive_rating },
          { label: 'Length', value: starship['length'] },
          { label: 'Manufacturer', value: starship.manufacturer },
          { label: 'Max atmosphering speed', value: starship.max_atmosphering_speed },
          { label: 'Model', value: starship.model },
          { label: 'Passengers', value: starship.passengers },
          { label: 'Starship class', value: starship.starship_class }
        ]
        return (
          <div key={index}  className="Starship">
            <div className="Starship__title">{starship.name}</div>
            <List list={list} />
          </div>
        )
      }) }
    </div>
  )
}

export default Starships
