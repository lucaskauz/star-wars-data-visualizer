import React, { useEffect, useState } from 'react'

import peopleApi from '../../services/people'

import Pagination from '../../components/Pagination/Pagination'

import './Characters.scss'

function Characters (props) {
  const {
    setCurrentCharacterUrl,
    currentCharacterUrl
  } = props

  const [characters, setCharacters] = useState([])

  useEffect(() => {
    peopleApi.getResourceList(setCharacters)
  }, [])

  const charactersList = characters.map((character, index) => {
    return (
      <div
        key={index}
        className={`Character ${currentCharacterUrl === character.url ? 'Character--active': ''}`}
        onClick={() => setCurrentCharacterUrl(character.url)}
        style={{
          backgroundImage: 'url(https://starwarsblog.starwars.com/wp-content/uploads/2019/08/rise-of-skywalker-poster-tall.jpg)'
        }}
      >
        <div className="Character__name">
          {character.name}
        </div>
      </div>
    )
  })

  return (
    <div className="Characters">
      <div className="Characters__list">
        { charactersList }
      </div>
      <Pagination
        next={peopleApi.next && (() => peopleApi.getNextResource(setCharacters)) }
        previous={peopleApi.previous && (() => peopleApi.getPreviousResource(setCharacters)) }
        pages={{
          total: peopleApi.getTotalPages(),
          current: peopleApi.getCurrentPage(),
          callback: (page) => peopleApi.getResourceList(setCharacters, page)
        }}/>
    </div>
  )
}

export default Characters
