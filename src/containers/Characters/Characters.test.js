import React from 'react'
import { mount } from 'enzyme'

import Characters from './Characters'

jest.mock('../../services/people', () => ({
  getResourceList: jest.fn().mockImplementation((callback) => {
    const defaultCharacter = {
      name: 'Lucas',
      url: 'url'
    }

    callback([defaultCharacter, defaultCharacter, defaultCharacter])
  }),
  getTotalPages: jest.fn(),
  getCurrentPage: jest.fn()
}))

const setCurrentCharacterUrl = jest.fn()

const defaultProps = {
  setCurrentCharacterUrl: setCurrentCharacterUrl
}

const setup = (props = defaultProps, shallow = true) => (
  shallow ?
    shallow(<Characters {...props} />) :
    mount(<Characters {...props} />)
)

describe('When it successfully loads 3 characters', () => {
  const component = setup(defaultProps, false)

  it('Render three characters ', () => {
    expect(component.find('.Character')).toHaveLength(3)
  });

  it('Changes the selected character when a character gets clicked',() => {
    component.find('.Character').first().simulate('click')

    expect(setCurrentCharacterUrl).toHaveBeenCalledWith('url')
  })
})
