import React, { useEffect, useState } from 'react';
import './App.scss';

import peopleApi from '../../services/people'

import Characters from '../Characters/Characters'
import MainCharacter from '../../components/MainCharacter/MainCharacter'

import starWarsLogo from '../../assets/images/star_wars.svg'

function App() {
  const [character, setCharacter] = useState(null)
  const [currentCharacterUrl, setCurrentCharacterUrl] = useState(null)

  useEffect(() => {
    if (currentCharacterUrl) {
      peopleApi.getResource(currentCharacterUrl, setCharacter)
    }
  },[currentCharacterUrl])

  return (
    <div className="App">
      <header className="App__header">
        <img src={starWarsLogo} alt="Star Wars" className="App__logo"/>
        <h1 className="App__title">Data Visualizer</h1>
      </header>
      <div className="App__content">
        <Characters setCurrentCharacterUrl={setCurrentCharacterUrl} currentCharacterUrl={currentCharacterUrl}/>
        { character && <MainCharacter character={character} clearCharacter={() => {
          setCharacter(null)
          setCurrentCharacterUrl(null)
        }}/> }
      </div>
    </div>
  );
}

export default App;
