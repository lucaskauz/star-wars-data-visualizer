# Star Wars Data Visualizer
This project aims to list the data from the swapi with some structure.

## Install
To install the project you must have node in your computer, that said you just need to run
```sh
npm install
```

## Running
```sh
npm start
```

## Testing
```sh
npm run test
```

## What this project has
- It's responsive
- Loads the characters starship
- Pagination
- One covered file

## What it could do better
- Good test coverage
- Loading states
- Failed state
- Have real images
